import sys
import paramiko
from scp import SCPClient

def scp_server(server, username, keyfile, source_file, remote_file, remote_path):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname=server, username=username, key_filename=keyfile)
    try:
        with SCPClient(ssh.get_transport()) as scp:
            scp.put(source_file, remote_path + "/" + remote_file)
        return [True, "Source file '" + source_file + "' to '" + username + "@" + server + ":" + remote_path + "/" + remote_file + "' copied correctly:"]
    except:
        return [False, "Source file '" + source_file + "' to '" + username + "@" + server + ":" + remote_path + "/" + remote_file + "' copied correctly:"]

def sys_argv_params():
    options = {
            "server": "",
            "username": "",
            "keyfile": "",
            "source_file": "",
            "remote_file": "",
            "remote_path": ""
        }
    try:
        options = {
            "server": sys.argv[1],
            "username": sys.argv[2],
            "keyfile": sys.argv[3],
            "source_file": sys.argv[4],
            "remote_file": sys.argv[5],
            "remote_path": sys.argv[6]
        }
        return options
    except IndexError:
        print("You must provide values in this order:")
        i = 1
        for key in options:
            print(i, ". ", key, sep="")
            i += 1
        exit()

def main():
    credentials = sys_argv_params()
    # print(credentials)
    copy_file = scp_server(
        credentials["server"], 
        credentials["username"], 
        credentials["keyfile"],
        credentials["source_file"],
        credentials["remote_file"],
        credentials["remote_path"]
    )
    print(copy_file[1], copy_file[0])

if __name__ == "__main__":
    main()