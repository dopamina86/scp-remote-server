import os # library for Operative System calls
import sys # library for inputs at the script invocation
import paramiko # library for SSH
from scp import SCPClient # library for SCP

# Function for copy file with SCP
def scp_server_file(server, username, keyfile, source_file, remote_file, remote_path):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname=server, username=username, key_filename=keyfile)
    try:
        with SCPClient(ssh.get_transport()) as scp:
            scp.put(source_file, remote_path + "/" + remote_file)
        return True
    except:
        return False

# Function for copy folder with SCP
def scp_server_folder(server, username, keyfile, source_folder, remotepath):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname=server, username=username, key_filename=keyfile)
    try:
        with SCPClient(ssh.get_transport()) as scp:
            scp.put(source_folder, recursive=True, remote_path=remotepath)
        return True
    except:
        return False

# Function for set values from the script invocation 
def sys_argv_params():
    options = {
        "server": "", # servername or IP
        "username": "", # username
        "keyfile": "", # keyfile
        "remote_path": "", # remote path
        "avoid": [] # files or folders to NOT copy
    }
    # validation for input user paramater at script invocation 
    try:
        options = {
            "server": sys.argv[1],
            "username": sys.argv[2],
            "keyfile": sys.argv[3],
            "remote_path": sys.argv[4]
        }
    except IndexError:
        print("You must provide values in this order:")
        i = 1
        for key in options:
            print(i, ". ", key, sep="")
            i += 1
        exit()
    # validation for input user paramater, files or folders to NOT copy, at script invocation 
    try:
        options = {
            "server": sys.argv[1],
            "username": sys.argv[2],
            "keyfile": sys.argv[3],
            "remote_path": sys.argv[4],
            "avoid": sys.argv[5:] # files or folders to NOT copy, at script invocation 
        }
    except IndexError:
        pass
    return options

def main():
    credentials = sys_argv_params()
    current_script = os.path.basename(__file__)
    # print(credentials)
    ls_path = os.listdir(os.getcwd()) #list of the current folder where the was invoked
    for files in ls_path:
        # avoid copy files or folders specify at script invocation
        if files in credentials["avoid"]:
            pass
        # copy files at the current folder of the script invocation
        elif os.path.isfile(files) and files != current_script:
            copy_file = scp_server_file(
                credentials["server"], 
                credentials["username"], 
                credentials["keyfile"],
                files,
                files,
                credentials["remote_path"]
            )
            print("Source file '" + files + "' to '" + credentials["username"] + "@" + 
                credentials["server"] + ":" + credentials["remote_path"] + "/" + files + 
                "' was copied correctly:", copy_file)
        # copy folders recursivily at the current folder of the script invocation
        elif not os.path.isfile(files):
            copy_folder = scp_server_folder(
                credentials["server"], 
                credentials["username"], 
                credentials["keyfile"],
                files,
                credentials["remote_path"]
            )
            print("Source folder '" + files + "' to '" + credentials["username"] + "@" + 
                credentials["server"] + ":" + credentials["remote_path"] + "/" + files + 
                "' was copied correctly:", copy_folder)

if __name__ == "__main__":
    main()